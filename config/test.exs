use Mix.Config

# Configure your database
config :quoteme, QuoteMe.Repo,
  username: "postgres",
  password: "",
  database: "quoteme_test",
  hostname: "quoteme-db",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :quoteme, QuoteMeWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
