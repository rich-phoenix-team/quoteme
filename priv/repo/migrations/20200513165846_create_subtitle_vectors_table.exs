defmodule QuoteMe.Repo.Migrations.CreateSubtitleVectorsTable do
  use Ecto.Migration

  def change do
    create table(:subtitle_vectors) do
      add :search_vector, :tsvector
      add :subtitle_id, references(:subtitles, on_delete: :delete_all), null: false
      add :movie_id, references(:movies, on_delete: :delete_all), null: false
    end

    create index(:subtitle_vectors, [:search_vector], using: :gin)
    create index(:subtitle_vectors, [:movie_id])
    create unique_index(:subtitle_vectors, [:subtitle_id])

  end
end
