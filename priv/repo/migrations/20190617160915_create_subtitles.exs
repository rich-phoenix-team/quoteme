defmodule QuoteMe.Repo.Migrations.CreateSubtitles do
  use Ecto.Migration

  def change do
    create table(:subtitles) do
      add :language, :string, null: false
      add :format, :string, null: false
      add :cd_number, :integer
      add :cd_total, :integer
      add :sub_file_id, :integer, null: false
      add :content, :text, null: false
      add :movie_id, references(:movies, on_delete: :delete_all), null: false

      timestamps()
    end

    create unique_index(:subtitles, [:sub_file_id])
    create index(:subtitles, [:movie_id])
    create index(:subtitles, [:language])
  end
end
