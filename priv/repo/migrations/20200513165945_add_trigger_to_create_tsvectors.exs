defmodule QuoteMe.Repo.Migrations.AddTriggerToCreateTsvectors do
  use Ecto.Migration

  def up do
    execute(
      """
      CREATE EXTENSION IF NOT EXISTS unaccent
      """
    )

    execute(
      """
      CREATE OR REPLACE FUNCTION create_tsvector()
      RETURNS TRIGGER AS $$
      BEGIN
        INSERT INTO subtitle_vectors (subtitle_id, movie_id, search_vector)
        VALUES (NEW.id, NEW.movie_id, to_tsvector('simple', unaccent(NEW.content)))
        ON CONFLICT (subtitle_id) do UPDATE SET search_vector = EXCLUDED.search_vector;
        RETURN NULL;
      END;
      $$ LANGUAGE plpgsql;
      """
    )

    execute(
      """
      CREATE CONSTRAINT TRIGGER create_tsvector
      AFTER INSERT OR UPDATE
      ON subtitles
      INITIALLY DEFERRED
      FOR EACH ROW
      EXECUTE PROCEDURE create_tsvector();
      """
    )

    execute("SET enable_seqscan = OFF")

  end

  def down do
    execute("SET enable_seqscan = ON")
    execute("DROP TRIGGER create_tsvector ON subtitles")
    execute("DROP FUNCTION create_tsvector()")
  end
end
