defmodule QuoteMe.Repo.Migrations.CreateMovies do
  use Ecto.Migration

  def change do
    create table(:movies) do
      add :name, :string, null: false
      add :imdb_parent_id, :integer
      add :season, :integer
      add :episode, :integer

      timestamps()
    end

  end
end
