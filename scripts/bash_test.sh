base_folder_name=files
base_folder_path=~/subtitle-data/$base_folder_name/0/0

converted_folder_name=converted_files
converted_folder_path=~/subtitle-data/$converted_folder_names/0/0

timestamp_pattern='\d+\s+\d{2}:\d{2}:\d{2},\d{3} --> \d{2}:\d{2}:\d{2},\d{3}\s+'
echo "recreating folder structure"
for d in $(find $base_folder_path -type d -regex '/.*[0-9a-z].*'); do
    new_path=$(echo $d | sed "s/$base_folder_name/$converted_folder_name/")
    mkdir -p $new_path
done

echo "converting files"
for f in $(find $base_folder_path -type f -regex '.*/[0-9][0-9][0-9]*'); do
    echo $f
    encoding=$(uchardet $f)
    output=$(echo $f | sed "s/$base_folder_name/$converted_folder_name/")
    echo $output

    if [ $encoding == 'MAC-CENTRALEUROPE' ]; then
        encoding=MACCENTRALEUROPE
    fi
    if [ $encoding == 'unknown' ]; then
        encoding=ASCII
    fi
    # -p is for print, so I can pipe it to iconv
    # -e is expression
    # -0 is so it loads whole file, not line by line
    perl -0pe 's/\s+/ /g;' -e "s/$timestamp_pattern//g" $f | iconv -c -f $encoding -t UTF-8 | tr -d '\000' > $output

    # retval=$?
    # if (( $retval > 0 )); then
    #     echo $f
    #     rm "$converted_folder_path/$basename"
    # # elif [ $retval > 0 ]; then
    # #     echo "error for file $f"
    # #     cp $f "$failed_folder/$(basename $f)"
    # fi
done