# QuoteMe

Using PostgreSQL Full Text Search
Context: Cinema
Models: Movie, Subtitles

Movies:
id, name, imdb_parent_id, season, episode

Subtitles:
id, language, format, cd_number, cd_total, opensubtitles_id, content