defmodule QuoteMeWeb.SearchView do
  use QuoteMeWeb, :view
  import QuoteMe.SearchUtil, only: [strip_query: 1]
end
