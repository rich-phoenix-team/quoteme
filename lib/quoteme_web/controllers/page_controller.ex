defmodule QuoteMeWeb.PageController do
  use QuoteMeWeb, :controller

  alias QuoteMe.Cinema

  def index(conn, _params) do
    subtitle_count = Cinema.count_subtitles()
    movie_count = Cinema.count_movies()
    render(conn, "index.html", subtitle_count: subtitle_count, movie_count: movie_count)
  end
end
