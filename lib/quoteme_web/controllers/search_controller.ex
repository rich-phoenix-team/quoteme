defmodule QuoteMeWeb.SearchController do
  use QuoteMeWeb, :controller

  alias QuoteMe.Cinema

  def index(conn, %{"query" => search_query}) do
    search_results = Cinema.search(search_query)
    render(conn, "index.html", search_results: search_results, search_query: search_query)
  end

  # def show(conn, %{"id" => id}) do
  #   search = Cinema.get_search!(id)
  #   render(conn, "show.html", search: search)
  # end

end
