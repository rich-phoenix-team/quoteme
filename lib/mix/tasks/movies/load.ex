defmodule Mix.Tasks.Movies.Load do
  use Mix.Task
  alias QuoteMe.Cinema

  @csv_export_path "tmp/subtitle_sample/mini-tab-export.csv"

  def run(_) do
    Mix.Task.run("app.start")
    {:ok, contents} = File.read(@csv_export_path)
    contents
    |> String.split("\n", trim: true)
    |> add_row_to_database
  end
# TODO sometimes value in csv is NULL, when its supposed to be empty
# FIX OPTION 1: replace all NULL with ""
# FIX OPTION 2: save only name and imdb_id and go to IMDB API for aditional info like season, episode, parent, ...
  defp add_row_to_database([]), do: []
  defp add_row_to_database([head | tail]) do
    [_, _, _, _, _, _, _, name, _, imdb_id, _, _, _imdb_parent_id, _season, _episode, _] = String.split(head, "\t")
    IO.puts "adding movie #{name} - ##{imdb_id} to DB"
    case Cinema.create_movie(%{
      name: name,
      id: imdb_id,
      # imdb_parent_id: imdb_parent_id,
      # season: season,
      # episode: episode
      }) do
        {:error, changeset} ->
        IO.puts "Error inserting movie to database. #{name} - ##{imdb_id}"
        IO.inspect changeset
        _ ->
        IO.puts "Movie inserted"
    end
    add_row_to_database(tail)
  end

end