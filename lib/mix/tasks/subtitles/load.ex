defmodule Mix.Tasks.Subtitles.Load do
  use Mix.Task
  alias QuoteMe.Cinema
  alias QuoteMe.Utils.EncodingConverter

  @csv_export_path "tmp/subtitle_sample/mini-tab-export.csv"
  @subtitle_root_path "tmp/subtitle_sample/subtitles"

  def run(_) do
    Mix.Task.run("app.start")
    {:ok, contents} = File.read(@csv_export_path)
    contents
    |> String.split("\n", trim: true)
    |> add_row_to_database
  end

  defp add_row_to_database([]), do: []
  defp add_row_to_database([head | tail]) do
    [_, sub_file_id, language, _, _, _, format, _, _, _, _, _, _, _season, _episode, _] = String.split(head, "\t")
    rel_file_path = Path.join(@subtitle_root_path, sub_file_id)
    if File.exists?(rel_file_path) do 
      IO.puts "adding #{rel_file_path} to DB"
      case Cinema.create_subtitle(%{
          language: language,
          format: format,
          sub_file_id: sub_file_id,
          content: get_subtitle_content(rel_file_path),
          movie_id: 1}) do
          {:error, changeset} ->
            IO.puts "Error inserting subbtitle to database. File: #{rel_file_path}"
            IO.inspect changeset
          _ ->
            IO.puts "Subtitle inserted"
        end
    else
      IO.puts "#{rel_file_path} does not exist"
    end
    add_row_to_database(tail)
  end

  defp get_subtitle_content(rel_file_path) do
    EncodingConverter.get_utf_8_content(rel_file_path)
  end
end