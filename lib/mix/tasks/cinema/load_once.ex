defmodule Mix.Tasks.Cinema.LoadOnce do
  use Mix.Task
  alias QuoteMe.Cinema
  alias QuoteMe.Repo

  @csv_export_path "tmp/subtitle_sample/export.txt"
  @subtitle_root_path "tmp/subtitle_sample/converted_files"
  @chunk_size 5000

  def run(_) do
    Mix.Task.run("app.start")
    Repo.transaction(fn ->
      Repo.delete_all Cinema.Movie
    end, timeout: :infinity)
    {:ok, contents} = File.read(@csv_export_path)
    IO.puts QuoteMe.Benchmark.measure(fn -> do_everything(contents) end)
  end

  defp do_everything(contents) do
    Repo.transaction(fn ->
      contents
      |> String.split("\n", trim: true)
      |> Stream.chunk_every(@chunk_size)
      |> Enum.map(&insert_all/1)
    end, timeout: :infinity)
  end

  defp create_movie_subtitle_tuple(row) do
    row2 = String.split(row, "\t")
    [_, sub_file_id, language, _, _, _, format, name, _, imdb_id | _tail] = row2
    rel_file_path = build_path(sub_file_id) 
    if File.exists?(rel_file_path) do 
      with {imdb_id, ""} <- Integer.parse(imdb_id),
        {sub_file_id, ""} <- Integer.parse(sub_file_id),
        {:ok, content} <- get_subtitle_content(rel_file_path)
      do
        movie = %{name: name, id: imdb_id, inserted_at: naive_time_now(), updated_at: naive_time_now()}
        subtitle = %{language: language, format: format, sub_file_id: sub_file_id, content: content, movie_id: imdb_id, inserted_at: naive_time_now(), updated_at: naive_time_now()}
        {movie, subtitle}
      else
        _ -> {nil, nil}
      end
    else
      {nil, nil}
    end
  end

  defp no_elem_is_nil({movie, subtitle}), do: !is_nil(movie) && !is_nil(subtitle)

  defp insert_all(rows) do
    rows=
      Enum.map(rows, fn row -> 
        create_movie_subtitle_tuple(row)
      end)
      |> Enum.filter(&no_elem_is_nil/1)
    movie_rows = Enum.map(rows, fn {mov, _sub} -> mov end)
    subtitle_rows = Enum.map(rows, fn {_mov, sub} -> sub end)

    {inserted_movies, _} = Repo.insert_all(Cinema.Movie, movie_rows, on_conflict: :nothing, conflict_target: :id)
    {inserted_subtitles, _} = Repo.insert_all(Cinema.Subtitle, subtitle_rows)
    IO.puts "inserted #{inserted_movies} movies and #{inserted_subtitles} subtitles"
  end

  defp naive_time_now() do
    NaiveDateTime.utc_now
    |> NaiveDateTime.truncate(:second)
  end

  def get_subtitle_content(rel_file_path) do
    File.read(rel_file_path)
  end

  defp build_path(sub_file_id) do
    folder_structure = get_folder_structure(sub_file_id)
    [@subtitle_root_path, folder_structure, sub_file_id]
    |> List.flatten
    |> Path.join
  end

  # defp get_folder_structure(_), do: ""
  defp get_folder_structure(file_name) do
    dir_depth = min(4, String.length(file_name))
    file_name
    |> String.slice(-dir_depth, dir_depth)
    |> String.reverse
    |> String.split("", trim: true)
  end

  defp remove_subtitle_files([]), do: :ok
  defp remove_subtitle_files([%{sub_file_id: sub_file_id} | tail]) do
    file_path = build_path("#{sub_file_id}")
    case File.rm(file_path) do
      :ok -> "file #{file_path} removed"
      {:error, reason} -> IO.puts "error removing file: #{Atom.to_string(reason)}"
    end
    remove_subtitle_files(tail)
  end

end