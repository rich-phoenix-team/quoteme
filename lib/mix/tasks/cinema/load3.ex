defmodule Mix.Tasks.Cinema.Load3 do
  use Mix.Task
  alias QuoteMe.Cinema
  alias QuoteMe.Repo

  # @csv_export_path "tmp/subtitle_sample/mini-tab-export.csv"
  # @subtitle_root_path "tmp/subtitle_sample/mini-export"
  @csv_export_path "tmp/subtitle_sample/export.txt"
  @subtitle_root_path "tmp/subtitle_sample/converted_files"
  @chunk_size 500

  def run(_) do
    Mix.Task.run("app.start")
    Repo.transaction(fn ->
      Repo.delete_all Cinema.Movie
    end, timeout: :infinity)
    {:ok, contents} = File.read(@csv_export_path)
    IO.puts QuoteMe.Benchmark.measure(fn -> do_everything(contents) end)
  end

  defp do_everything(contents) do
    {movies, subtitles} =
    contents
    |> String.split("\n", trim: true)
    |> Enum.reduce({[], []}, &add_row/2)

    IO.puts QuoteMe.Benchmark.measure(fn ->
      Repo.transaction(fn ->
        movies
        |> Enum.chunk_every(@chunk_size)
        |> Enum.each(fn chunk -> Repo.insert_all(Cinema.Movie, chunk, on_conflict: :nothing, conflict_target: :id) end)
      end, timeout: :infinity)
    end)

    IO.puts QuoteMe.Benchmark.measure(fn ->
        subtitles
        |> Enum.chunk_every(@chunk_size)
        |> Enum.each(fn chunk ->
          Repo.transaction(fn ->
            Repo.insert_all(Cinema.Subtitle, chunk, on_conflict: {:replace_all_except, [:inserted_at]}, conflict_target: :sub_file_id)
          end, timeout: :infinity)
          remove_subtitle_files(chunk)
        end)
    end)
  end

  defp naive_time_now() do
    NaiveDateTime.utc_now
    |> NaiveDateTime.truncate(:second)
  end

  defp add_row(row, {movies, subtitles}) do
    row2 = String.split(row, "\t")
    [_, sub_file_id, language, _, _, _, format, name, _, imdb_id | _tail] = row2
    rel_file_path = build_path(sub_file_id)
    if File.exists?(rel_file_path) do 
      with {imdb_id, ""} <- Integer.parse(imdb_id),
        {sub_file_id, ""} <- Integer.parse(sub_file_id)
      do
        movie = %{name: name, id: imdb_id, inserted_at: naive_time_now(), updated_at: naive_time_now()}
        subtitle = %{language: language, format: format, sub_file_id: sub_file_id, content: get_subtitle_content(rel_file_path), movie_id: imdb_id, inserted_at: naive_time_now(), updated_at: naive_time_now()}
        {[movie | movies], [subtitle | subtitles]}
      else
        _ -> {movies, subtitles}
      end
    else
      {movies, subtitles}
    end
  end

  def get_subtitle_content(rel_file_path) do
    {:ok, contents} = File.read(rel_file_path)
    contents
  end

  defp build_path(sub_file_id) do
    folder_structure = get_folder_structure(sub_file_id)
    [@subtitle_root_path, folder_structure, sub_file_id]
    |> List.flatten
    |> Path.join
  end

  defp get_folder_structure(_), do: ""
  defp get_folder_structure(file_name) do
    dir_depth = min(4, String.length(file_name))
    file_name
    |> String.slice(-dir_depth, dir_depth)
    |> String.reverse
    |> String.split("", trim: true)
  end

  defp remove_subtitle_files([]), do: :ok
  defp remove_subtitle_files([%{sub_file_id: sub_file_id} | tail]) do
    file_path = build_path("#{sub_file_id}")
    case File.rm(file_path) do
      :ok -> "file #{file_path} removed"
      {:error, reason} -> IO.puts "error removing file: #{Atom.to_string(reason)}"
    end
    remove_subtitle_files(tail)
  end

end