defmodule Mix.Tasks.Cinema.Load do
  use Mix.Task
  alias QuoteMe.Cinema
  alias QuoteMe.Utils.EncodingConverter

  @csv_export_path "tmp/subtitle_sample/export.txt"
  @subtitle_root_path "tmp/subtitle_sample/converted_files"

  def run(_) do
    Mix.Task.run("app.start")
    QuoteMe.Repo.delete_all Cinema.Movie
    {:ok, contents} = File.read(@csv_export_path)
    IO.puts QuoteMe.Benchmark.measure(fn -> do_everything(contents) end)
    # do_everything(contents)
  end

  defp do_everything(contents) do
    contents
    |> String.split("\n", trim: true)
    |> add_row_to_database
  end

  defp add_row_to_database([]), do: []
  defp add_row_to_database([head | tail]) do
  # IO.puts QuoteMe.Benchmark.measure(fn -> 
    # [_, sub_file_id, language, _, _, _, format, name, _, imdb_id, _, _] = String.split(head, "\t")
    row2 = String.split(head, "\t")
    [_, sub_file_id, language, _, _, _, format, name, _, imdb_id, _, _ | tail2] = row2
    rel_file_path = build_path(sub_file_id)
    if File.exists?(rel_file_path) do 
      with {imdb_id, ""} <- Integer.parse(imdb_id),
        {sub_file_id, ""} <- Integer.parse(sub_file_id)
      do
        IO.puts "adding movie #{name} - ##{imdb_id} and subtitles to DB. sub_file_id: #{sub_file_id}"
        with {:ok, %{id: id}} <- Cinema.create_movie(%{name: name, id: imdb_id}),
          {:ok, _} <- Cinema.create_subtitle(%{language: language, format: format, sub_file_id: sub_file_id, content: get_subtitle_content(rel_file_path), movie_id: id})
        do
          "a"
          # IO.puts "successfully added #{id}"
        else
          {:error, changeset} ->
            IO.puts "error adding movie or subtitle. sub_file_id: #{sub_file_id}"
            IO.inspect changeset
          _ -> IO.puts "error happened"
        end
      end
    # else
    #   IO.puts "#{rel_file_path} does not exist"
    end
  #  end)
    add_row_to_database(tail)
  end

  def get_subtitle_content(rel_file_path) do
    # EncodingConverter.get_utf_8_content(rel_file_path)
    {:ok, contents} = File.read(rel_file_path)
    contents
  end

  defp build_path(sub_file_id) do
    folder_structure = get_folder_structure(sub_file_id)
    [@subtitle_root_path, folder_structure, sub_file_id]
    |> List.flatten
    |> Path.join
  end

  defp get_folder_structure(file_name), do: ""
  defp get_folder_structure(file_name) do
    file_name
    |> String.slice(-4, 4)
    |> String.reverse
    |> String.split("", trim: true)
  end
end