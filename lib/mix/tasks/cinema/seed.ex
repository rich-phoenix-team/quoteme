defmodule Mix.Tasks.Cinema.Seed do
  use Mix.Task
  alias QuoteMe.Cinema
  alias QuoteMe.Repo
  import Ecto.Query

  @file_path "tmp/subtitle_sample/mini-export/1951620000"
  @movie_id 99887766
  @chunk_size 500

  def run(_) do
    Mix.Task.run("app.start")
    from(m in Cinema.Movie, where: m.id == @movie_id) |> Repo.delete_all
    Cinema.create_movie %{id: @movie_id, name: "random"}
    content = File.read!(@file_path)
    IO.puts QuoteMe.Benchmark.measure(fn -> do_everything(content) end)
  end

  defp do_everything(content) do
    time = naive_time_now()
    subtitles = 
      Enum.map(1..50_000, fn sub_file_id ->
        %{language: "eng", format: "srt", sub_file_id: sub_file_id, content: content, movie_id: @movie_id, inserted_at: time, updated_at: time}
      end)

    IO.puts QuoteMe.Benchmark.measure(fn ->
        subtitles
        |> Enum.chunk_every(@chunk_size)
        |> Enum.each(fn chunk ->
          Repo.transaction(fn ->
            Repo.insert_all(Cinema.Subtitle, chunk, on_conflict: :nothing)
          end, timeout: :infinity)
        end)
    end)
  end

  defp naive_time_now() do
    NaiveDateTime.utc_now
    |> NaiveDateTime.truncate(:second)
  end

end