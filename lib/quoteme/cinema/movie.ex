defmodule QuoteMe.Cinema.Movie do
  use Ecto.Schema
  import Ecto.Changeset
# TODO implement to_string

  schema "movies" do
    field :episode, :integer
    field :imdb_parent_id, :integer
    field :name, :string
    field :season, :integer
    has_many :subtitles, QuoteMe.Cinema.Subtitle

    timestamps()
  end

  @doc false
  def changeset(movie, attrs) do
    movie
    |> cast(attrs, [:id, :name, :imdb_parent_id, :season, :episode])
    |> validate_required([:name, :id])
  end
end
