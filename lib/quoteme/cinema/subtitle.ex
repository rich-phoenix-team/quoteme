defmodule QuoteMe.Cinema.Subtitle do
  use Ecto.Schema
  import Ecto.Changeset
# TODO implement to_string

  schema "subtitles" do
    field :cd_number, :integer
    field :cd_total, :integer
    field :content, :binary
    field :format, :string
    field :language, :string
    field :sub_file_id, :integer
    belongs_to :movie, QuoteMe.Cinema.Movie

    timestamps()
  end

  @doc false
  def changeset(subtitle, attrs) do
    subtitle
    |> cast(attrs, [:cd_number, :cd_total, :content, :format, :language, :sub_file_id, :movie_id])
    |> validate_required([:movie_id, :content, :format, :language, :sub_file_id])
    |> assoc_constraint(:movie)
  end
end
