defmodule QuoteMe.Cinema.SearchResult do
    defstruct [:movie_id, :name, :highlight]
end