defmodule QuoteMe.Cinema do
  @moduledoc """
  The Cinema context.
  """

  import Ecto.Query, warn: false
  alias QuoteMe.Repo
  import Ecto.Query
  alias QuoteMe.Cinema.Movie
  alias QuoteMe.Cinema.SearchResult
  import QuoteMe.SearchUtil, only: [normalize_query: 1]

  @doc """
  Returns the list of movies.

  ## Examples

      iex> list_movies()
      [%Movie{}, ...]

  """
  def list_movies do
    Repo.all(Movie)
  end

  def count_movies do
    Repo.aggregate(Movie, :count, :id, [])
  end

  @doc """
  Gets a single movie.

  Raises `Ecto.NoResultsError` if the Movie does not exist.

  ## Examples

      iex> get_movie!(123)
      %Movie{}

      iex> get_movie!(456)
      ** (Ecto.NoResultsError)

  """
  def get_movie!(id), do: Repo.get!(Movie, id)

  def get_movie_by_imdb_id(imdb_id), do: Repo.get_by(Movie, id: imdb_id)

  @doc """
  Creates a movie.

  ## Examples

      iex> create_movie(%{field: value})
      {:ok, %Movie{}}

      iex> create_movie(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_movie(attrs \\ %{}) do
    %Movie{}
    |> Movie.changeset(attrs)
    |> Repo.insert(on_conflict: {:replace_all_except, [:inserted_at, :id]}, conflict_target: :id)
  end

  @doc """
  Updates a movie.

  ## Examples

      iex> update_movie(movie, %{field: new_value})
      {:ok, %Movie{}}

      iex> update_movie(movie, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_movie(%Movie{} = movie, attrs) do
    movie
    |> Movie.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Movie.

  ## Examples

      iex> delete_movie(movie)
      {:ok, %Movie{}}

      iex> delete_movie(movie)
      {:error, %Ecto.Changeset{}}

  """
  def delete_movie(%Movie{} = movie) do
    Repo.delete(movie)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking movie changes.

  ## Examples

      iex> change_movie(movie)
      %Ecto.Changeset{source: %Movie{}}

  """
  def change_movie(%Movie{} = movie) do
    Movie.changeset(movie, %{})
  end

  alias QuoteMe.Cinema.Subtitle

  def count_subtitles do
    Repo.aggregate(Subtitle, :count, :id, [])
  end

  @doc """
  Returns the list of subtitles.

  ## Examples

      iex> list_subtitles()
      [%Subtitle{}, ...]

  """
  def list_subtitles do
    Repo.all(Subtitle)
  end

  @doc """
  Gets a single subtitle.

  Raises `Ecto.NoResultsError` if the Subtitle does not exist.

  ## Examples

      iex> get_subtitle!(123)
      %Subtitle{}

      iex> get_subtitle!(456)
      ** (Ecto.NoResultsError)

  """
  def get_subtitle!(id), do: Repo.get!(Subtitle, id)

  @doc """
  Creates a subtitle.

  ## Examples

      iex> create_subtitle(%{field: value})
      {:ok, %Subtitle{}}

      iex> create_subtitle(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_subtitle(attrs \\ %{}) do
    %Subtitle{}
    |> Subtitle.changeset(attrs)
    |> Repo.insert(on_conflict: {:replace_all_except, [:inserted_at]}, conflict_target: :sub_file_id)
  end

  def create_subtitle_for_movie(%Movie{} = movie, attrs \\ %{}) do
    %Subtitle{}
    |> Subtitle.changeset(attrs)
    |> Ecto.Changeset.put_assoc(:movie, movie)
    |> Repo.insert(on_conflict: {:replace_all_except, [:inserted_at, :id]}, conflict_target: :sub_file_id)
  end

  @doc """
  Updates a subtitle.

  ## Examples

      iex> update_subtitle(subtitle, %{field: new_value})
      {:ok, %Subtitle{}}

      iex> update_subtitle(subtitle, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_subtitle(%Subtitle{} = subtitle, attrs) do
    subtitle
    |> Subtitle.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Subtitle.

  ## Examples

      iex> delete_subtitle(subtitle)
      {:ok, %Subtitle{}}

      iex> delete_subtitle(subtitle)
      {:error, %Ecto.Changeset{}}

  """
  def delete_subtitle(%Subtitle{} = subtitle) do
    Repo.delete(subtitle)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking subtitle changes.

  ## Examples

      iex> change_subtitle(subtitle)
      %Ecto.Changeset{source: %Subtitle{}}

  """
  def change_subtitle(%Subtitle{} = subtitle) do
    Subtitle.changeset(subtitle, %{})
  end


  defp preload_movies(subtitles_or_subtitles) do
    Repo.preload(subtitles_or_subtitles, :movie)
  end

  def search_movies(formated_query) do
    Movie
    |> limit(1)
    |> Repo.all

  end
  
  defmacro subtitle_search(formated_query) do
    quote do
      fragment(
        """
        SELECT DISTINCT ON (movie_id) movie_id, subtitle_id, to_tsquery('simple', ?) as q
        FROM subtitle_vectors
        WHERE search_vector @@ to_tsquery('simple', ?)
        """,
        unquote(formated_query),
        unquote(formated_query)
      )
    end
  end

  def search(search_query) do
    search_query
    |> normalize_query
    |> search_formated
    |> Repo.all
  end

  defp search_formated(formated_query) do
    from sub in Subtitle,
    join: sub_id_and_query in subtitle_search(^formated_query),
    on: sub_id_and_query.subtitle_id == sub.id,
    join: mov in Movie,
    on: mov.id == sub.movie_id,
    select: %SearchResult{
      movie_id: mov.id,
      name: mov.name, 
      highlight: fragment("ts_headline('simple', ?, q, 'MaxFragments = 1, MinWords = 20')", sub.content)
    },
    limit: 10
  end

end
