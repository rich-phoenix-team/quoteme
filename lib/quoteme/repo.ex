defmodule QuoteMe.Repo do
  use Ecto.Repo,
    otp_app: :quoteme,
    adapter: Ecto.Adapters.Postgres
end
