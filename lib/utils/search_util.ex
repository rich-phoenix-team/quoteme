defmodule QuoteMe.SearchUtil do

  def strip_query(search_query), do: String.replace(search_query, ~r/[^\w\s]/, " ")

  def to_tsquery(stripped_query) do
    stripped_query
    |> String.split
    |> Enum.join(" <-> ")
  end

  def normalize_query(search_query) do
    search_query
    |> strip_query
    |> to_tsquery
  end

end