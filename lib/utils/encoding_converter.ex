defmodule QuoteMe.Utils.EncodingConverter do
  @uchardet_to_iconv_encodings %{
    "MAC-CENTRALEUROPE" => "MACCENTRALEUROPE",
    "ascii/unknown" => "ASCII"
  }

  def get_file_encoding(full_path) do
    case System.cmd("uchardet", [full_path]) do
      {encoding, 0} -> String.trim(encoding)
      {encoding, _} ->
        IO.puts "Uchardet error. File: #{full_path}"
        String.trim(encoding)
    end
  end


  def get_iconv_result("", _), do: ""
  def get_iconv_result(iconv_encoding, full_path) do
    case System.cmd("iconv", ["-f #{iconv_encoding}", "-t UTF-8", full_path]) do
      {utf8, 0} -> String.trim(utf8)
      {utf8, _} ->
        IO.puts "Iconv error. File: #{full_path}"
        String.trim(utf8)
    end
  end

  def uchardet_to_iconv_encoding(uchardet_encoding) do
    Map.get(@uchardet_to_iconv_encodings, uchardet_encoding, uchardet_encoding)
  end

  def get_utf_8_content(full_path) do
    full_path
    |> get_file_encoding
    |> uchardet_to_iconv_encoding
    |> get_iconv_result(full_path)
  end

end