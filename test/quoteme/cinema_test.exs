defmodule QuoteMe.CinemaTest do
  use QuoteMe.DataCase

  alias QuoteMe.Cinema

  describe "movies" do
    alias QuoteMe.Cinema.Movie

    @valid_attrs %{episode: 42, id: "some imdb_id", imdb_parent_id: "some imdb_parent_id", name: "some name", season: 42}
    @update_attrs %{episode: 43, id: "some updated imdb_id", imdb_parent_id: "some updated imdb_parent_id", name: "some updated name", season: 43}
    @invalid_attrs %{episode: nil, id: nil, imdb_parent_id: nil, name: nil, season: nil}

    def movie_fixture(attrs \\ %{}) do
      {:ok, movie} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Cinema.create_movie()

      movie
    end

    test "list_movies/0 returns all movies" do
      movie = movie_fixture()
      assert Cinema.list_movies() == [movie]
    end

    test "get_movie!/1 returns the movie with given id" do
      movie = movie_fixture()
      assert Cinema.get_movie!(movie.id) == movie
    end

    test "create_movie/1 with valid data creates a movie" do
      assert {:ok, %Movie{} = movie} = Cinema.create_movie(@valid_attrs)
      assert movie.episode == 42
      assert movie.id == "some imdb_id"
      assert movie.imdb_parent_id == "some imdb_parent_id"
      assert movie.name == "some name"
      assert movie.season == 42
    end

    test "create_movie/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Cinema.create_movie(@invalid_attrs)
    end

    test "update_movie/2 with valid data updates the movie" do
      movie = movie_fixture()
      assert {:ok, %Movie{} = movie} = Cinema.update_movie(movie, @update_attrs)
      assert movie.episode == 43
      assert movie.imdb_id == "some updated imdb_id"
      assert movie.imdb_parent_id == "some updated imdb_parent_id"
      assert movie.name == "some updated name"
      assert movie.season == 43
    end

    test "update_movie/2 with invalid data returns error changeset" do
      movie = movie_fixture()
      assert {:error, %Ecto.Changeset{}} = Cinema.update_movie(movie, @invalid_attrs)
      assert movie == Cinema.get_movie!(movie.id)
    end

    test "delete_movie/1 deletes the movie" do
      movie = movie_fixture()
      assert {:ok, %Movie{}} = Cinema.delete_movie(movie)
      assert_raise Ecto.NoResultsError, fn -> Cinema.get_movie!(movie.id) end
    end

    test "change_movie/1 returns a movie changeset" do
      movie = movie_fixture()
      assert %Ecto.Changeset{} = Cinema.change_movie(movie)
    end
  end

  describe "subtitles" do
    alias QuoteMe.Cinema.Subtitle

    @valid_attrs %{cd_number: 42, cd_total: 42, content: "some content", format: "some format", language: "some language"}
    @update_attrs %{cd_number: 43, cd_total: 43, content: "some updated content", format: "some updated format", language: "some updated language"}
    @invalid_attrs %{cd_number: nil, cd_total: nil, content: nil, format: nil, language: nil}

    def subtitle_fixture(attrs \\ %{}) do
      {:ok, subtitle} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Cinema.create_subtitle()

      subtitle
    end

    test "list_subtitles/0 returns all subtitles" do
      subtitle = subtitle_fixture()
      assert Cinema.list_subtitles() == [subtitle]
    end

    test "get_subtitle!/1 returns the subtitle with given id" do
      subtitle = subtitle_fixture()
      assert Cinema.get_subtitle!(subtitle.id) == subtitle
    end

    test "create_subtitle/1 with valid data creates a subtitle" do
      assert {:ok, %Subtitle{} = subtitle} = Cinema.create_subtitle(@valid_attrs)
      assert subtitle.cd_number == 42
      assert subtitle.cd_total == 42
      assert subtitle.content == "some content"
      assert subtitle.format == "some format"
      assert subtitle.language == "some language"
    end

    test "create_subtitle/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Cinema.create_subtitle(@invalid_attrs)
    end

    test "update_subtitle/2 with valid data updates the subtitle" do
      subtitle = subtitle_fixture()
      assert {:ok, %Subtitle{} = subtitle} = Cinema.update_subtitle(subtitle, @update_attrs)
      assert subtitle.cd_number == 43
      assert subtitle.cd_total == 43
      assert subtitle.content == "some updated content"
      assert subtitle.format == "some updated format"
      assert subtitle.language == "some updated language"
    end

    test "update_subtitle/2 with invalid data returns error changeset" do
      subtitle = subtitle_fixture()
      assert {:error, %Ecto.Changeset{}} = Cinema.update_subtitle(subtitle, @invalid_attrs)
      assert subtitle == Cinema.get_subtitle!(subtitle.id)
    end

    test "delete_subtitle/1 deletes the subtitle" do
      subtitle = subtitle_fixture()
      assert {:ok, %Subtitle{}} = Cinema.delete_subtitle(subtitle)
      assert_raise Ecto.NoResultsError, fn -> Cinema.get_subtitle!(subtitle.id) end
    end

    test "change_subtitle/1 returns a subtitle changeset" do
      subtitle = subtitle_fixture()
      assert %Ecto.Changeset{} = Cinema.change_subtitle(subtitle)
    end
  end
end
